@echo off

:: 현재 스크립트의 경로로 이동 (스크립트가 실행된 위치에서 상대 경로를 사용하기 위해)
cd /d %~dp0
:: 경로 출력
echo current Path : %~dp0
:: 파일이 존재하는지 확인
if not exist hooks\post-checkout (
    echo "hooks\post-checkout 파일을 찾을 수 없습니다."
    exit /b 1
)

:: Git hooks 디렉토리가 존재하는지 확인
if not exist .git\hooks\ (
    echo ".git\hooks 디렉토리를 찾을 수 없습니다."
    exit /b 1
)

:: Git Hook이 있는 경로로 복사
if exist .git\hooks\ (
    copy hooks\post-checkout .git\hooks\post-checkout
    echo Hook file Copy Finish.

) else (
    echo ".git/hooks 디렉토리가 존재하지 않습니다."
)
